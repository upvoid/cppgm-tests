#include <cstdlib>
#include <iostream>
#include <ctime>
#include <random>

char randomChar()
{
    int min = 32;
    int max = 126;
    return min + rand() % (max - min + 1);
}

int main()
{
    srand(std::random_device()());
    for (int l = 0; l < 3; ++l)
    {
    for (int i = 0; i < 100; ++i)
        std::cout << randomChar();
    std::cout << std::endl;
    }
}
