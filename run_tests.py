#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
run_tests.py
~~~~~~~~~~~~
This program runs all tests in the test directories
and compares the output and exit code to the given
reference result.
"""

import argparse
import glob
import os
import sys

import envoy

__version__ = "1.0.0"
__author__ = "Tobias Berling"


COLORED_SUCCESS = '\033[92mSUCCESS\033[0m'
COLORED_FAILURE = '\033[91mFAILURE\033[0m'


class TestResult:
    def __init__(self, name, tests_dir, ref=False):
        self.name = name
        self.tests_dir = tests_dir
        self.my_output_file = None
        self.my_exit_status_file = None
        self.ref_output_file = None
        self.ref_exit_status_file = None
        self.status = -1

        self.check_and_load_file_paths(ref)

    def check_and_load_file_paths(self, ref=False):
        self.test_file = self.__check_file('t')
        self.ref_output_file = self.__check_file('ref', not ref)
        self.ref_exit_status_file = self.__check_file('ref.exit_status', not ref)
        self.my_output_file = self.__check_file('my', False)
        self.my_exit_status_file = self.__check_file('my.exit_status', False)

    def __check_file(self, extension, check_existence=True):
        filename = '{}.{}'.format(self.name, extension)
        filepath = os.path.abspath(os.path.join(self.tests_dir, filename))
        if check_existence:
            assert os.path.isfile(filepath), '{}  missing'.format(filename)
        return filepath

    def __str__(self):
        return 'Test ' + self.name


class TestRunner:
    def __init__(self, args):
        self.__longest_test_name = 0
        self.stats = {'success': 0, 'failure': 0, 'total': 0}

        self.tests_dirs = args.tests_directory

        assert os.path.isfile(args.executable), \
            'Executable not found'
        assert os.access(args.executable, os.X_OK), \
            'Executable is not executable'
        self.executable = os.path.abspath(args.executable)

        self.load_tests(args.ref)
        if args.ref:
            self.create_refs()
        else:
            self.run_tests()

    def load_tests(self, ref=False):
        self.tests = []
        for tests_dir in self.tests_dirs:
            assert os.path.isdir(tests_dir), \
                'Test directory does not exist'

            for test in sorted(os.listdir(tests_dir)):
                if test.endswith('.t'):
                    if len(test[:-2]) > self.__longest_test_name:
                        self.__longest_test_name = len(test[:-2])
                    self.tests.append(TestResult(test[:-2], tests_dir, ref))

    def create_refs(self):
        print ''
        for test in self.tests:
            self.run_test(test, True)
            print ' Creating {}.{{ref, ref.exit_status}}'.format(test.name)

        print ''

    def run_tests(self):
        for test in self.tests:
            self.run_test(test)

        print ''
        print ' {} | Status '.format('Test'.ljust(self.__longest_test_name + 4))
        print '-' * (self.__longest_test_name + 6) + '+' + '-' * 30

        for test in self.tests:
            self.compare_test(test)
        print ''

        if self.stats['failure'] == 0:
            print ' ALL TESTS PASSED'
        elif self.stats['success'] == 0:
            print ' ALL TESTS FAILED'
        else:
            no_success = self.stats['success']
            no_failure = self.stats['failure']
            print ' {} PASSED TEST{}'.format(no_success, '' if no_success == 1 else 'S')
            print ' {} FAILED TEST{}'.format(no_failure, '' if no_failure == 1 else 'S')
        print ''

    def run_test(self, test, ref=False):
        tmp_output_file = test.my_output_file if not ref else test.ref_output_file
        tmp_exit_status_file = test.my_exit_status_file if not ref else test.ref_exit_status_file

        with open(test.test_file) as input_file:
            r = envoy.run(self.executable, data=input_file.read(), timeout=2)
            test.status = r.status_code

        with open(tmp_output_file, 'w+') as output_file:
            output_file.write(r.std_out)
            if r.std_err:
                output_file.write(r.std_err)

        with open(tmp_exit_status_file, 'w+') as output_file:
            if r.status_code == 0:
                output_file.write('EXIT_SUCCESS\n')
            else:
                output_file.write('EXIT_FAILURE\n')

    def compare_test(self, test):
        r = envoy.run('shasum ' + test.ref_exit_status_file)
        ref_exit_status_hash = r.std_out.split(' ')[0]
        r = envoy.run('shasum ' + test.my_exit_status_file)
        my_exit_status_hash = r.std_out.split(' ')[0]

        r = envoy.run('shasum ' + test.ref_output_file)
        ref_output_hash = r.std_out.split(' ')[0]
        r = envoy.run('shasum ' + test.my_output_file)
        my_output_hash = r.std_out.split(' ')[0]

        exit_status_check = ref_exit_status_hash == my_exit_status_hash
        if test.status == 0:
            output_check = ref_output_hash == my_output_hash
        else:
            output_check = True

        prefix = ' {} |'.format(test.name.ljust(self.__longest_test_name + 4))

        if exit_status_check and output_check:
            test.status = 0
            print '{} {}'.format(prefix, COLORED_SUCCESS)
            self.stats['total'] += 1
            self.stats['success'] += 1
        else:
            test.status = 1
            tmp = '-1'
            if not exit_status_check and not output_check:
                tmp = 'EXIT_STATUS, OUTPUT'
            elif not exit_status_check:
                tmp = 'EXIT_STATUS'
            elif not output_check:
                tmp = 'OUTPUT'
            print '{} {} ({})'.format(prefix, COLORED_FAILURE, tmp)
            self.stats['total'] += 1
            self.stats['failure'] += 1


if __name__ == '__main__':
    executable = None
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Script for running CPPGM tests',
        epilog='example(s):\n'
               '    {0} ../cppgm-pa1/pptoken pa1\n'
               '    {0} ../cppgm-pa1/pptoken ../cppgm-pa1/tests pa1\n'
               '    {0} -r ../cppgm-pa1/pptoken-ref pa1\n\n'
               'exit status:\n'
               '    The exit code equals the number of failed tests.'
               ''.format(sys.argv[0])
        )
    parser.add_argument('executable', action='store', metavar='EXECUTABLE',
                        help='Path to the binary taking the test as input')
    parser.add_argument('tests_directory', metavar='TESTS_DIRECTORY', nargs='+',
                        help='Directory containing the test files '
                             '(test.t, test.ref, and test.ref.exit_status)')
    parser.add_argument('-r', '--ref', action='store_true',
                        help='create refs file instead of comparing results')
    args = parser.parse_args()

    runner = TestRunner(args)

    sys.exit(runner.stats['failure'])
